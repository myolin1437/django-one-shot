from django.urls import path

# from todos.views import (
#     list_todos,
#     show_todolist,
#     create_todolist,
#     update_todolist,
#     delete_todolist,
#     create_todoitem,
#     update_todoitem,
# )

from todos.views import (
    ToDoListView,
    TodoDetailView,
    ToDoCreateView,
    ToDoUpdateView,
    ToDoDeleteView,
    ItemCreateView,
    ItemUpdateView,
)

# urlpatterns = [
#     path("", list_todos, name="list_todos"),
#     path("<int:pk>/", show_todolist, name="show_todolist"),
#     path("create/", create_todolist, name="create_todolist"),
#     path("<int:pk>/edit/", update_todolist, name="update_todolist"),
#     path("<int:pk>/delete/", delete_todolist, name="delete_todolist"),
#     path("items/create/", create_todoitem, name="create_todoitem"),
#     path("items/<int:pk>/edit/", update_todoitem, name="update_todoitem"),
# ]

urlpatterns = [
    path("", ToDoListView.as_view(), name="list_todos"),
    path("<int:pk>/", TodoDetailView.as_view(), name="show_todolist"),
    path("create/", ToDoCreateView.as_view(), name="create_todolist"),
    path("<int:pk>/edit/", ToDoUpdateView.as_view(), name="update_todolist"),
    path("<int:pk>/delete/", ToDoDeleteView.as_view(), name="delete_todolist"),
    path("items/create/", ItemCreateView.as_view(), name="create_todoitem"),
    path(
        "items/<int:pk>/edit/",
        ItemUpdateView.as_view(),
        name="update_todoitem",
    ),
]
