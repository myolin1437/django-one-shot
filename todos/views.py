from django.shortcuts import get_object_or_404, render, redirect
from django.urls import reverse_lazy

from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView

from todos.models import TodoList, TodoItem

from todos.forms import TodoListForm, TodoItemForm


# Create your views here.

############### LIST VIEW ###############
# def list_todos(request):
#     todolist_list = TodoList.objects.all()
#     context = {
#         "todolist_list": todolist_list,
#     }

#     return render(request, "todos/list.html", context)


class ToDoListView(ListView):
    model = TodoList
    template_name = "todos/list.html"


############### DETAIL VIEW ###############
# def show_todolist(request, pk):
#     todolist = get_object_or_404(TodoList, pk=pk)
#     # todolist = TodoList.objects.get(pk=pk)
#     context = {
#         "todolist": todolist,
#     }

#     return render(request, "todos/detail.html", context)


class TodoDetailView(DetailView):
    model = TodoList
    template_name = "todos/detail.html"


############### CREATE VIEW ###############
# def create_todolist(request):
#     form = TodoListForm()
#     if request.method == "POST":
#         form = TodoListForm(request.POST)
#         if form.is_valid():
#             todolist = form.save()
#             return redirect("show_todolist", pk=todolist.pk)
#     context = {
#         "form": form,
#     }

#     return render(request, "todos/create.html", context)


# def create_todolist(request):
#     if request.method == "POST" and TodoListForm:
#         form = TodoListForm(request.POST)
#         if form.is_valid():
#             todolist = form.save()
#             return redirect("show_todolist", pk=todolist.pk)
#     elif TodoListForm:
#         form = TodoListForm()
#     else:
#         form = None
#     context = {
#         "form": form,
#     }

#     return render(request, "todos/create.html", context)


class ToDoCreateView(CreateView):
    model = TodoList
    template_name = "todos/create.html"
    fields = ["name"]

    def get_success_url(self):
        return reverse_lazy("show_todolist", args=[self.object.pk])


############### UPDATE VIEW ###############
# def update_todolist(request, pk):
#     todolist = TodoList.objects.get(pk=pk)
#     if request.method == "POST":
#         form = TodoListForm(request.POST, instance=todolist)
#         if form.is_valid():
#             form.save()
#             return redirect("show_todolist", pk=pk)
#     else:
#         form = TodoListForm(instance=todolist)
#     context = {
#         "form": form,
#     }

#     return render(request, "todos/update.html", context)


class ToDoUpdateView(UpdateView):
    model = TodoList
    template_name = "todos/update.html"
    fields = ["name"]

    def get_success_url(self):
        return reverse_lazy("show_todolist", args=[self.object.pk])


############### DELETE VIEW ###############
# def delete_todolist(request, pk):
#     todolist = get_object_or_404(TodoList, pk=pk)
#     # todolist = TodoList.objects.get(pk=pk)
#     if request.method == "POST":
#         todolist.delete()
#         return redirect("list_todos")
#     context = {}

#     return render(request, "todos/delete.html", context)


class ToDoDeleteView(DeleteView):
    model = TodoList
    template_name = "todos/delete.html"
    success_url = reverse_lazy("list_todos")


############### CREATE TODOITEM VIEW ###############
# def create_todoitem(request):
#     form = TodoItemForm()
#     if request.method == "POST":
#         form = TodoItemForm(request.POST)
#         if form.is_valid():
#             todoitem = form.save()
#             return redirect("show_todolist", pk=todoitem.list.pk)
#     context = {
#         "form": form,
#     }

#     return render(request, "todos/create_todoitem.html", context)


class ItemCreateView(CreateView):
    model = TodoItem
    template_name = "todos/create_todoitem.html"
    fields = ["task", "due_date", "is_completed", "list"]

    def get_success_url(self):
        return reverse_lazy("show_todolist", args=[self.object.list.pk])


############### UPDATE TODOITEM VIEW ###############
# def update_todoitem(request, pk):
#     todoitem = TodoItem.objects.get(pk=pk)
#     if request.method == "POST":
#         form = TodoItemForm(request.POST, instance=todoitem)
#         if form.is_valid():
#             form.save()
#             return redirect("show_todolist", pk=todoitem.list.pk)
#     form = TodoItemForm(instance=todoitem)
#     context = {
#         "form": form,
#     }

#     return render(request, "todos/update_todoitem.html", context)


class ItemUpdateView(UpdateView):
    model = TodoItem
    template_name = "todos/update_todoitem.html"
    fields = ["task", "due_date", "is_completed", "list"]

    def get_success_url(self):
        return reverse_lazy("show_todolist", args=[self.object.list.pk])
